# transf

Minimal TCP-based file transfer.

## Usage

Example usage to send a file `hello.txt` from `hostA` to `hostB`:

Listen on hostA:
```
root@hostA $ transf recv 0.0.0.0:6969 received_hello.txt
```

Send from hostB:
```
root@hostB $ transf send <host>:6969 hello.txt
```

Help text:
```
Usage: transf <METHOD> <ADDRESS> [FILE]

Arguments:
  <METHOD>   [possible values: send, recv]
  <ADDRESS>  
  [FILE]     

Options:
  -o, --one-shot
  -h, --help     Print help
  -V, --version  Print version
```
If the FILE argument is not provided, uses stdin for sending and stdout for receiving.

## Protocol

The `transf` protocol is minimal.

### Header

| 7 Bytes |
| ------ |
| `transf:` |

### Body

Tranferring a file of `n` bytes:

| `n` Bytes |
| ------ |
| (file contents) |

### Begin/End

The start of the stream begins with the header (`transf:`), and ends when the connection is closed. One file is transferred per connection.
