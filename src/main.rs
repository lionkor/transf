use std::{
    fs::File,
    io::{copy, stdin, stdout, Read, Write},
    net::{SocketAddr, TcpListener, TcpStream, ToSocketAddrs},
    path::PathBuf,
    process::exit,
};

use clap::{Parser, ValueEnum};

#[derive(ValueEnum, Clone)]
enum Method {
    Send,
    Recv,
}

#[derive(Parser)]
#[command(author, version, about)]
struct Args {
    #[arg(value_enum)]
    method: Method,
    address: String,
    file: Option<PathBuf>,
    #[arg(short, long, default_value_t = false)]
    one_shot: bool,
    #[arg(short, long, default_value_t = false)]
    quiet: bool,
}

fn send(addrs: &Vec<SocketAddr>, file: &Option<PathBuf>) {
    for addr in addrs {
        match TcpStream::connect(addr) {
            Ok(mut stream) => {
                let bytes = &"transf:".as_bytes();
                if let Err(err) = stream.write_all(bytes) {
                    eprintln!("Error: Failed to write 'transf:' to stream: {}", err);
                    exit(1);
                }
                let mut reader: Box<dyn Read> = match file {
                    Some(real_file) => Box::from(match File::open(real_file) {
                        Ok(val) => val,
                        Err(err) => {
                            eprintln!("Error: Failed to open file {:?}: {}", real_file, err);
                            exit(1);
                        }
                    }),
                    None => Box::from(stdin()),
                };
                if let Err(err) = copy(&mut reader, &mut stream) {
                    eprintln!("Error: Failed to copy from file/stdin to TCP: {}", err);
                    exit(1);
                }
                return;
            }
            Err(_) => {
                continue;
            }
        }
    }
    eprintln!(
        "Error: Resolved hostname to {} addresses, but failed to connect to any of them",
        addrs.len()
    );
    eprintln!("FYI the resolved addresses were: {:?}", addrs);
}

fn recv(addrs: &Vec<SocketAddr>, file: &Option<PathBuf>, one_shot: bool) {
    let listener = (|| {
        for addr in addrs {
            match TcpListener::bind(addr) {
                Ok(sock) => return sock,
                Err(_) => continue,
            }
        }
        eprintln!(
            "Error: Resolved hostname to {} addresses, but failed to bind to any of them",
            addrs.len()
        );
        eprintln!("FYI the resolved addresses were: {:?}", addrs);
        exit(1);
    })();
    eprintln!("Listening on {}", listener.local_addr().unwrap());
    eprintln!("One-shot enabled - will shut down after first successful receive");
    for stream in listener.incoming() {
        let mut stream = stream.unwrap();
        let mut id: [u8; 7] = [0; 7];
        match stream.read_exact(&mut id) {
            Ok(_) => {
                let as_string = String::from_utf8(id.to_vec()).unwrap_or(String::from(""));
                if as_string == "transf:" {
                    let mut writer: Box<dyn Write> = match file {
                        Some(real_file) => Box::from(File::create(real_file).unwrap()),
                        None => Box::from(stdout()),
                    };
                    if let Err(err) = copy(&mut stream, &mut writer) {
                        eprintln!("Error: Failed to copy from TCP to file/stdout: {}", err);
                        continue;
                    }
                    eprintln!("EOT");
                } else {
                    continue;
                }
                if one_shot {
                    return;
                }
            }
            Err(err) => {
                eprintln!("Error: Ignoring connection due to error: {}", err);
                continue;
            }
        }
    }
}

fn main() {
    let args = Args::parse();

    let addrs: Vec<SocketAddr> = match args.address.to_socket_addrs() {
        Ok(addrs) => addrs.collect(),
        Err(err) => {
            eprintln!(
                "Error: Could not resolve {} as a <host>:<port> or similar: {}",
                args.address, err
            );
            exit(1);
        }
    };

    match args.method {
        Method::Send => send(&addrs, &args.file),
        Method::Recv => recv(&addrs, &args.file, args.one_shot),
    }
}
